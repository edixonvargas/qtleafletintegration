//access_token = 'pk.eyJ1Ijoic25vcmZhbG9ycGFndXMiLCJhIjoic1oxRTMxcyJ9.eC6cjtLmFs9EcVwmT1isOQ';
access_token = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';

var map = L.map('map').setView([-34.488689, -63.983238], 13);
//L.tileLayer('https://{s}.tiles.mapbox.com/v4/examples.map-i86nkdio/{z}/{x}/{y}@2x.png?access_token='+access_token, {
//    maxZoom: 18,
//    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
//        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
//        'Imagery &copy; <a href="http://mapbox.com">Mapbox</a>',
//    id: 'examples.map-i86nkdio',
//}).addTo(map);
var tileLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png?access_token='+access_token, {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery &copy; <a href="http://mapbox.com">Mapbox</a>',
    id: 'examples.map-i86nkdio',
}).addTo(map);

//var marker = L.marker(map.getCenter()).addTo(map);
//marker.bindPopup("Hello World!").openPopup();

var markers = L.layerGroup();
var polyline = L.polyline([], {color: 'red'});
var curPanIndex = 0;

if(typeof MainWindow != 'undefined') {
    var onMapMove = function() { MainWindow.onMapMove(map.getCenter().lat, map.getCenter().lng) };
    map.on('move', onMapMove);
    onMapMove();
}

function onClick(e){
    var quadcopter = L.icon({
                                iconUrl: 'images/multicopter-32.png',
                                //                     iconRetinaUrl: 'my-icon@2x.png',
                                iconSize: [32, 32]
                                //                                    iconAnchor: [22, 94],
//                                popupAnchor: [-3, -76]
                                //                     shadowUrl: 'my-icon-shadow.png',
                                //                     shadowRetinaUrl: 'my-icon-shadow@2x.png',
                                //                     shadowSize: [68, 95],
                                //                     shadowAnchor: [22, 94]})
                            });
    if(adding == 'polyline'){
        if(!markers.hasLayer(polyline)){
            polyline.addTo(markers);
        }

        polyline.addLatLng(e.latlng);
        L.circleMarker(e.latlng, {color : 'red'}).setRadius(5).addTo(markers);

//        items.addLayer(polyline);
//        map.addLayer(polyline);
//        if(clicks == 0){
//            polyline =
//        }else{
//            polyline = L.polyline([e.latlng], {color: 'red'}).addTo(map);
////            polyline.addLatLng(e.latlng);
//        }
    }else if(adding == 'point'){
        L.marker(e.latlng).addTo(markers);

    }else if(adding == 'popup'){
        var popup = L.popup()
            .setLatLng(e.latlng)
            .setContent('<p>This is a marker<br />This is a nice popup.</p>')
            .openOn(map);
    }

//    marker = L.marker(e.latlng, {icon : quadcopter}
//             ).addTo(markers);
    map.addLayer(markers);
//    markers.push(marker);
//    map.addLayer(markers[markers.length - 1]);
}

function onMoveEnd(e){
    if(playing){
        curPanIndex++;
        console.log('move end');
        var points = polyline.getLatLngs();
        console.log(curPanIndex + ' ' + points.length);
        if(curPanIndex < points.length){
            map.panTo(points[curPanIndex], {duration: 10, easeLinearity: 1});
        }else{
            map.off('moveend', onMoveEnd);
        }
    }
}
