#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QtWebkit>
#include <QtWebKitWidgets>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();

	private slots:

		void on_actionCreate_path_toggled(bool chk);

		void on_actionAddPoint_toggled(bool chk);

		void on_actionClearMap_triggered();

		void on_actionPlay_triggered();

		void on_actionAddPopup_toggled(bool chk);

	private:
		Ui::MainWindow *ui;

		QWebView *viewer;

};

#endif // MAINWINDOW_H
