#-------------------------------------------------
#
# Project created by QtCreator 2016-07-08T22:35:37
#
#-------------------------------------------------

QT       += core gui webkit webkitwidgets

QMAKE_CXX += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LeafletIntegration
TEMPLATE = app


SOURCES += main.cpp\
		mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

DISTFILES += \
    map.js \
    map.html

RESOURCES += \
    data.qrc
