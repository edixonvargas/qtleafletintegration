#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

	ui->setupUi(this);

	viewer = new QWebView();
	QString
			mapHtmlPath = "C:\\Users\\Edixon\\Google Drive\\Programacion\\C++\\Qt\\LeafletIntegration\\Source\\map.html";


	QFileInfo mapFile(mapHtmlPath);

	if(mapFile.exists()){
		viewer->load(QUrl::fromLocalFile(mapHtmlPath));
		qDebug() << "map loaded";
	}else{
		qDebug() << "map not loaded";
	}

//	//This will open Inspector window and fix it to be half of the screen width
//	QDesktopWidget *dw = QApplication::desktop();

//	setGeometry(dw->width() / 2, 0, dw->width()/2, dw->height()/2);

//	QWebInspector *inspector = new QWebInspector();
//	inspector->setPage(viewer->page());
//	inspector->setGeometry(0, 0, dw->width() / 2, dw->height()/2);
//	inspector->show();

//	QActionGroup *actions = new QActionGroup(this);

//	actions->addAction(ui->actionActionAddPoint);
//	actions->addAction(ui->actionCreate_path);
//	actions->addAction(ui->actionActionNone);

	setCentralWidget(viewer);

	connect(viewer, &QWebView::loadFinished, [=](bool ok)
	{
		QString mapJSPath = "C:\\Users\\Edixon\\Google Drive\\Programacion\\C++\\Qt\\LeafletIntegration\\Source\\map.js";
		QFile js(mapJSPath);
		if(ok){
			if(js.open(QFile::ReadOnly)){
				QString strJS(js.readAll());
				viewer->page()->mainFrame()->evaluateJavaScript(strJS);
				qDebug() << "js loaded";
			}else{
				qDebug() << "js not loaded";
			}
		}else{
			qDebug() << "js not loaded";
		}
	});

}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_actionCreate_path_toggled(bool chk)
{
	if(chk){
		viewer->page()->mainFrame()->evaluateJavaScript("adding = 'polyline';"
														"map.on('click', onClick);");
	}else{
		viewer->page()->mainFrame()->evaluateJavaScript("map.off('click', onClick);");
	}
	ui->actionAddPoint->setChecked(false);
	ui->actionAddPopup->setChecked(false);
}

void MainWindow::on_actionAddPoint_toggled(bool chk)
{
	if(chk){
		viewer->page()->mainFrame()->evaluateJavaScript("adding = 'point';"
														"map.on('click', onClick);");
	}else{
		viewer->page()->mainFrame()->evaluateJavaScript("map.off('click', onClick);");
	}
	ui->actionCreate_path->setChecked(false);
	ui->actionAddPopup->setChecked(false);
}

void MainWindow::on_actionClearMap_triggered()
{
	qDebug() << "clear";
	viewer->page()->mainFrame()->evaluateJavaScript("polyline.setLatLngs([]);"
													"markers.clearLayers();"
													/*"for(i=0; i < markers.lenght; i++){"
													"  map.removeLayer(markers[2]);"
													"}"*/);
}

void MainWindow::on_actionPlay_triggered()
{
//	viewer->settings()->setUserStyleSheetUrl(QUrl(""));
	viewer->page()->mainFrame()->evaluateJavaScript("var quadcopter = L.icon({"
													"iconUrl: 'images/multicopter-32.png',"
													//                     iconRetinaUrl: 'my-icon@2x.png',
													"iconSize: [32, 32]"
													//                                    iconAnchor: [22, 94],
					//                                popupAnchor: [-3, -76]
													//                     shadowUrl: 'my-icon-shadow.png',
													//                     shadowRetinaUrl: 'my-icon-shadow@2x.png',
													//                     shadowSize: [68, 95],
													//                     shadowAnchor: [22, 94]})
													"});"
													"map.panTo(polyline.getLatLngs()[0], {duration: 2});"
													"var playing = true;"
													"curPanIndex = 0;"
													"map.on('moveend', onMoveEnd);"
													/*"map.panTo(polyline.getLatLngs()[1],"
													"{"
													"  animate: true,"
													"  duration: 20,"
													"  easeLinearity: 1"
													"});"*/);

}



void MainWindow::on_actionAddPopup_toggled(bool chk)
{
	if(chk){
		viewer->page()->mainFrame()->evaluateJavaScript("adding = 'popup';"
														"map.on('click', onClick);");
	}else{
		viewer->page()->mainFrame()->evaluateJavaScript("map.off('click', onClick);");
	}
	ui->actionCreate_path->setChecked(false);
	ui->actionAddPoint->setChecked(false);
}
